This Online Bank Management System is an online software designed in Java programming language for the purpose of effective online banking system. It mainly aims at making the banking system easily accessible from anywhere and improving and enhancing the running banking process. With the use of this online software, bank customers don’t need to go to bank to make inquiry regarding their balance or to transfer their balance to other account or for any other banking services.
# ATM Banking System Project Abstract:
In banking system, different branches are present and these are connected to the main branch. So, the same bank is located at different locations for providing the same type of banking services. The manual method of managing these banks from one location is difficult, but it can be easily done with an ATM software application like this.

The overall banking procedure has become much easier, comfortable and secured as well with ATM (Automated Teller Machine). ATM has saved customers time for withdrawing balance, checking recent transactions and checking bank balance from any location.
With the proposed ATM Banking System, bank management is easier with a more efficient platform for managing customer details, managing their account and recording their transaction history.

This project can help students understand the operations that are involved in designing ATM software. The proposed system is good; it has minimum features, but further enhancements can be done by adding new modules, features and sub-systems into this project.
